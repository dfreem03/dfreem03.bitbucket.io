var searchData=
[
  ['get_78',['get',['../classtask__share_1_1Queue.html#af2aef1dd3eed21c4b6c2e601cb8497d4',1,'task_share.Queue.get()'],['../classtask__share_1_1Share.html#a599cd89ed1cd79af8795a51d8de70d27',1,'task_share.Share.get()']]],
  ['get_5fdelta_79',['get_delta',['../classencoderDriver_1_1encoderDriver.html#a5109c1111af1846776caffbbd75c376c',1,'encoderDriver.encoderDriver.get_delta()'],['../classEncoderDriver__week2_1_1EncoderDriver.html#a609045c0a6433563cfb11108f8cd4cde',1,'EncoderDriver_week2.EncoderDriver.get_delta()'],['../classEncoderDriver__week3_1_1EncoderDriver.html#a9972c5ea66eab753c7d0a278c02ff732',1,'EncoderDriver_week3.EncoderDriver.get_delta()'],['../shares__week3_8py.html#abddbf63b5a25824a214c9d0e91229a4e',1,'shares_week3.get_delta()']]],
  ['get_5fkp_80',['get_Kp',['../classCTRL__week3_1_1ClosedLoop.html#a51ce69d3dc4dc0a9d037c3195c1412ef',1,'CTRL_week3.ClosedLoop.get_Kp()'],['../classCTRL__week4_1_1ClosedLoop.html#ac2b287ed2cefeac34773adfc06955079',1,'CTRL_week4.ClosedLoop.get_Kp()']]],
  ['get_5fposition_81',['get_position',['../classencoderDriver_1_1encoderDriver.html#a378cc895e885af2f49a2eb12bf779e9f',1,'encoderDriver.encoderDriver.get_position()'],['../classEncoderDriver__week2_1_1EncoderDriver.html#a96d87c08b8732d871b2d28c4e9c30498',1,'EncoderDriver_week2.EncoderDriver.get_position()'],['../classEncoderDriver__week3_1_1EncoderDriver.html#a3c9aa6d274a472e7f84a04364c9a09f6',1,'EncoderDriver_week3.EncoderDriver.get_position()']]],
  ['get_5ftrace_82',['get_trace',['../classcotask_1_1Task.html#a6e51a228f985aec8c752bd72a73730ae',1,'cotask::Task']]],
  ['getchange_83',['getChange',['../vend_8py.html#afb98521d22e72b8fff0c7e7462532d72',1,'vend']]],
  ['go_84',['go',['../classcotask_1_1Task.html#a78e74d18a5ba94074c2b5309394409a5',1,'cotask::Task']]],
  ['go_5fflag_85',['go_flag',['../classcotask_1_1Task.html#a96733bb9f4349a3f284083d1d4e64f9f',1,'cotask::Task']]]
];
