var searchData=
[
  ['data_39',['data',['../0x03__nucleo_8py.html#a7f7b99fc71524180c37f766fee9d2373',1,'0x03_nucleo']]],
  ['datalist_40',['dataList',['../UI__week1_8py.html#a008a111c77165f64a244adddef14deef',1,'UI_week1']]],
  ['datastring_41',['dataString',['../UI__week1_8py.html#a3a6c552c666da8437ec1a2f7f0f00637',1,'UI_week1']]],
  ['datatask_42',['dataTask',['../dataTask_8py.html#a751efc17a80e862715e14fc506d50a71',1,'dataTask']]],
  ['datatask_2epy_43',['dataTask.py',['../dataTask_8py.html',1,'']]],
  ['delta_44',['delta',['../classEncoderDriver__week2_1_1EncoderDriver.html#abba202801da6f18c9938f6eb6a63960c',1,'EncoderDriver_week2.EncoderDriver.delta()'],['../classEncoderDriver__week3_1_1EncoderDriver.html#ae0f4c3f9d25997500bdd37ee4f1ff536',1,'EncoderDriver_week3.EncoderDriver.delta()']]],
  ['delta1_45',['delta1',['../classencoderDriver_1_1encoderDriver.html#a619b44c6d2cf4c5b5c4bd623a61541a5',1,'encoderDriver::encoderDriver']]],
  ['delta2_46',['delta2',['../classencoderDriver_1_1encoderDriver.html#a7ff9979294d0e3cb12698b9f3ce0d1f9',1,'encoderDriver::encoderDriver']]],
  ['disable_47',['disable',['../classMotorDriver_1_1DRV8847.html#a3395cf38f54ab1dc745f78bdedd1f728',1,'MotorDriver.DRV8847.disable()'],['../classMotorDriver__week3_1_1MotorDriver.html#ae18a845a7e645d7d9d2a5807d2b03a28',1,'MotorDriver_week3.MotorDriver.disable()']]],
  ['drpupper_48',['drpupper',['../vend_8py.html#a1d861ff4a1d48f1fa33abae589849245',1,'vend']]],
  ['drv8847_49',['DRV8847',['../classMotorDriver_1_1DRV8847.html',1,'MotorDriver']]],
  ['drv8847_5fchannel_50',['DRV8847_channel',['../classMotorDriver_1_1DRV8847__channel.html',1,'MotorDriver']]],
  ['dt_51',['dt',['../0x03__nucleo_8py.html#a779ac0db47d93b7ebe5c5088ac584a1b',1,'0x03_nucleo']]],
  ['duration_52',['duration',['../temperature__lab0x04_8py.html#aa2421ce94960a359255c3f8c73294a16',1,'temperature_lab0x04']]],
  ['derivation_20of_20equations_20of_20motion_20for_20ball_20on_20platform_53',['Derivation of Equations of Motion for Ball on Platform',['../pagehw2.html',1,'']]]
];
