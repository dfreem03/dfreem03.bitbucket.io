var files_dup =
[
    [ "0x03_nucleo.py", "0x03__nucleo_8py.html", "0x03__nucleo_8py" ],
    [ "0x03_UI.py", "0x03__UI_8py.html", "0x03__UI_8py" ],
    [ "ADC.py", "ADC_8py.html", "ADC_8py" ],
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO", "classBNO055_1_1BNO.html", "classBNO055_1_1BNO" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", "ControllerTask_8py" ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "CTRL_week3.py", "CTRL__week3_8py.html", [
      [ "ClosedLoop", "classCTRL__week3_1_1ClosedLoop.html", "classCTRL__week3_1_1ClosedLoop" ]
    ] ],
    [ "CTRL_week4.py", "CTRL__week4_8py.html", [
      [ "ClosedLoop", "classCTRL__week4_1_1ClosedLoop.html", "classCTRL__week4_1_1ClosedLoop" ]
    ] ],
    [ "dataTask.py", "dataTask_8py.html", "dataTask_8py" ],
    [ "encoderDriver.py", "encoderDriver_8py.html", [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "EncoderDriver_week2.py", "EncoderDriver__week2_8py.html", [
      [ "EncoderDriver", "classEncoderDriver__week2_1_1EncoderDriver.html", "classEncoderDriver__week2_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver_week3.py", "EncoderDriver__week3_8py.html", [
      [ "EncoderDriver", "classEncoderDriver__week3_1_1EncoderDriver.html", "classEncoderDriver__week3_1_1EncoderDriver" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", "encoderTask_8py" ],
    [ "EncoderTask_week3.py", "EncoderTask__week3_8py.html", [
      [ "CTRLtask", "classEncoderTask__week3_1_1CTRLtask.html", "classEncoderTask__week3_1_1CTRLtask" ]
    ] ],
    [ "Fibonacci.py", "Fibonacci_8py.html", "Fibonacci_8py" ],
    [ "FSM3.py", "FSM3_8py.html", [
      [ "FSM3", "classFSM3_1_1FSM3.html", "classFSM3_1_1FSM3" ]
    ] ],
    [ "HW0x02.py", "HW0x02_8py.html", "HW0x02_8py" ],
    [ "IMUTask.py", "IMUTask_8py.html", "IMUTask_8py" ],
    [ "LAB0x02.py", "LAB0x02_8py.html", "LAB0x02_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_week1.py", "main__week1_8py.html", "main__week1_8py" ],
    [ "main_week2.py", "main__week2_8py.html", "main__week2_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "MotorDriver_week3.py", "MotorDriver__week3_8py.html", [
      [ "MotorDriver", "classMotorDriver__week3_1_1MotorDriver.html", "classMotorDriver__week3_1_1MotorDriver" ]
    ] ],
    [ "MotorTask.py", "MotorTask_8py.html", "MotorTask_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "program.py", "program_8py.html", "program_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "shares_week3.py", "shares__week3_8py.html", "shares__week3_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "temperature_lab0x04.py", "temperature__lab0x04_8py.html", "temperature__lab0x04_8py" ],
    [ "ThinkFast_a.py", "ThinkFast__a_8py.html", "ThinkFast__a_8py" ],
    [ "ThinkFastB.py", "ThinkFastB_8py.html", "ThinkFastB_8py" ],
    [ "touchPanelDriver.py", "touchPanelDriver_8py.html", [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "touchPanelTask.py", "touchPanelTask_8py.html", "touchPanelTask_8py" ],
    [ "UI_Task_week3.py", "UI__Task__week3_8py.html", [
      [ "UI_Task", "classUI__Task__week3_1_1UI__Task.html", "classUI__Task__week3_1_1UI__Task" ]
    ] ],
    [ "UI_week1.py", "UI__week1_8py.html", "UI__week1_8py" ],
    [ "UI_week2.py", "UI__week2_8py.html", "UI__week2_8py" ],
    [ "UITask.py", "UITask_8py.html", "UITask_8py" ],
    [ "vend.py", "vend_8py.html", "vend_8py" ]
];