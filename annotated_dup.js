var annotated_dup =
[
    [ "ADC", null, [
      [ "internal", "classADC_1_1internal.html", "classADC_1_1internal" ]
    ] ],
    [ "BNO055", null, [
      [ "BNO", "classBNO055_1_1BNO.html", "classBNO055_1_1BNO" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "CTRL_week3", null, [
      [ "ClosedLoop", "classCTRL__week3_1_1ClosedLoop.html", "classCTRL__week3_1_1ClosedLoop" ]
    ] ],
    [ "CTRL_week4", null, [
      [ "ClosedLoop", "classCTRL__week4_1_1ClosedLoop.html", "classCTRL__week4_1_1ClosedLoop" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "EncoderDriver_week2", null, [
      [ "EncoderDriver", "classEncoderDriver__week2_1_1EncoderDriver.html", "classEncoderDriver__week2_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver_week3", null, [
      [ "EncoderDriver", "classEncoderDriver__week3_1_1EncoderDriver.html", "classEncoderDriver__week3_1_1EncoderDriver" ]
    ] ],
    [ "EncoderTask_week3", null, [
      [ "CTRLtask", "classEncoderTask__week3_1_1CTRLtask.html", "classEncoderTask__week3_1_1CTRLtask" ]
    ] ],
    [ "FSM3", null, [
      [ "FSM3", "classFSM3_1_1FSM3.html", "classFSM3_1_1FSM3" ]
    ] ],
    [ "mcp9808", null, [
      [ "coms", "classmcp9808_1_1coms.html", "classmcp9808_1_1coms" ]
    ] ],
    [ "MotorDriver", null, [
      [ "DRV8847", "classMotorDriver_1_1DRV8847.html", "classMotorDriver_1_1DRV8847" ],
      [ "DRV8847_channel", "classMotorDriver_1_1DRV8847__channel.html", "classMotorDriver_1_1DRV8847__channel" ]
    ] ],
    [ "MotorDriver_week3", null, [
      [ "MotorDriver", "classMotorDriver__week3_1_1MotorDriver.html", "classMotorDriver__week3_1_1MotorDriver" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchPanelDriver", null, [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "UI_Task_week3", null, [
      [ "UI_Task", "classUI__Task__week3_1_1UI__Task.html", "classUI__Task__week3_1_1UI__Task" ]
    ] ]
];