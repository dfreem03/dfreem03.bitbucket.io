var ThinkFastB_8py =
[
    [ "ICCap", "ThinkFastB_8py.html#a4c8d81ea3af274ac0f94b68115df0882", null ],
    [ "onOC", "ThinkFastB_8py.html#ac5da498d231b4089a56df0e9966c5d0e", null ],
    [ "_avgReac", "ThinkFastB_8py.html#ad03c1aaf8f7532654b29691cf37bb104", null ],
    [ "_ICcap", "ThinkFastB_8py.html#a8131bb216524b163b7b40cc9d8270122", null ],
    [ "_last_comp", "ThinkFastB_8py.html#aa5adce4455a6675e6901d8f03a2ca76c", null ],
    [ "_LightUp", "ThinkFastB_8py.html#afc5ab9290f08fe4e8feeae96d0fd0d73", null ],
    [ "_new_comp", "ThinkFastB_8py.html#ae02887b4fa97f582db95c1dbcec71b1c", null ],
    [ "_sstate", "ThinkFastB_8py.html#adb37b8ccf5b536b558d312f8dca14460", null ],
    [ "_state", "ThinkFastB_8py.html#ad584f65bf527967007214c306247a3b3", null ],
    [ "_TotReac", "ThinkFastB_8py.html#a949b47f1001b151a4dc4f1ca515f45d6", null ],
    [ "_x", "ThinkFastB_8py.html#ae4fa342f1cbf86e92d4d4b2299dec8d4", null ],
    [ "Button", "ThinkFastB_8py.html#abb46143572878a419c431bc0a2a13190", null ],
    [ "IC", "ThinkFastB_8py.html#acc48f5f782ad0ed6c213f48eada00df2", null ],
    [ "LED", "ThinkFastB_8py.html#aacfd23c5a4deaa18f120a0717048c4c1", null ],
    [ "OC", "ThinkFastB_8py.html#a4475b3818489f6a797649765ab85fce5", null ],
    [ "pB3", "ThinkFastB_8py.html#a78201b317132c09ebebaefb179de6623", null ],
    [ "tim2", "ThinkFastB_8py.html#ac5f6455836d043ba9a982b05399b5ab8", null ]
];