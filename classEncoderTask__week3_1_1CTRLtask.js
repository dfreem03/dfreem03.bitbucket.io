var classEncoderTask__week3_1_1CTRLtask =
[
    [ "__init__", "classEncoderTask__week3_1_1CTRLtask.html#ab44a6d9a146f0af433dca92910a9da61", null ],
    [ "run", "classEncoderTask__week3_1_1CTRLtask.html#a889cc245105860d770814e40eadd50a6", null ],
    [ "ClosedLoop", "classEncoderTask__week3_1_1CTRLtask.html#a5d7763eba94f911a6eb09bea76ab113f", null ],
    [ "EncoderDriver", "classEncoderTask__week3_1_1CTRLtask.html#accc5f90068585fa4ce104e559ca4c34b", null ],
    [ "i", "classEncoderTask__week3_1_1CTRLtask.html#a01446a58ba1ef9c111e579898e204314", null ],
    [ "MotorDriver", "classEncoderTask__week3_1_1CTRLtask.html#acc4421c5299e523264cc6a7a9738e55b", null ],
    [ "omega_meas", "classEncoderTask__week3_1_1CTRLtask.html#a13f10eca04e7cc050b3f629097380ce0", null ],
    [ "omega_ref", "classEncoderTask__week3_1_1CTRLtask.html#ac23bf554c7e29d57f8315a3b97fa82cc", null ],
    [ "split", "classEncoderTask__week3_1_1CTRLtask.html#a6daa05b242ba2281fe95487173685bab", null ],
    [ "step", "classEncoderTask__week3_1_1CTRLtask.html#af87dba3112b6d3a4bdca3e7deaa1c31b", null ],
    [ "string", "classEncoderTask__week3_1_1CTRLtask.html#a019c4c330b449df92fa847c39db8bff9", null ],
    [ "strip", "classEncoderTask__week3_1_1CTRLtask.html#aac5b04be4b5ffb078e7755c1d18b4131", null ],
    [ "theta_ref", "classEncoderTask__week3_1_1CTRLtask.html#ae05a028354a0c0f3db2e9eba8ddfb78a", null ],
    [ "thisTime", "classEncoderTask__week3_1_1CTRLtask.html#a3952e5cf828a6b9faea895bd4dc78a80", null ],
    [ "time_ref", "classEncoderTask__week3_1_1CTRLtask.html#a4406bb2d186a4907c2c07383b3b27cc1", null ]
];