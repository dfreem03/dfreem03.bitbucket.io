/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Daniel Freeman Mechatronics Portfolio", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "HW 0x02 Documentation", "index.html#sec_hw2", null ],
    [ "Lab 0x01 Documentation", "index.html#sec_lab1", null ],
    [ "Lab 0x02 Documentation", "index.html#sec_lab2", null ],
    [ "Lab 0x03 Documentation", "index.html#sec_lab3", null ],
    [ "Lab 0xFF, Week 1 Documentation", "index.html#sec_week1", null ],
    [ "Lab 0xFF, Week 2 Documentation", "index.html#sec_week2", null ],
    [ "Lab 0xFF, Week 3 Documentation", "index.html#sec_week3", null ],
    [ "Lab 0xFF, Week 4 Documentation", "index.html#sec_week4", null ],
    [ "ME 405 - Lab 0x01", "index.html#sec_lab1405", null ],
    [ "ME 405 - Lab 0x02", "index.html#sec_lab2405", null ],
    [ "Homework 2", "index.html#sec_hw2405", null ],
    [ "ME 405 - Lab 0x03", "index.html#sec_lab3405", null ],
    [ "ME 405 - HW 0x04", "index.html#sec_hw4405", null ],
    [ "ME 405 - Lab 0x04", "index.html#sec_lab4405", null ],
    [ "ME 405 - HW 0x05", "index.html#sec_hw5405", null ],
    [ "ME 405 - Term Project", "index.html#sec_termProject", null ],
    [ "Derivation of Equations of Motion for Ball on Platform", "pagehw2.html", null ],
    [ "Plots for simulation of balancing ball on platform", "pagehw4.html", null ],
    [ "The Math", "lab0xFF_math.html", null ],
    [ "The Drivers", "lab0xFF_drivers.html", null ],
    [ "The Tasks", "lab0xFF_tasks.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"0x03__UI_8py.html",
"classMotorDriver_1_1DRV8847__channel.html#af968eefdd07b201b58b0583a06a3de54"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';