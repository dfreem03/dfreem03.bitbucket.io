var shares_8py =
[
    [ "calib", "shares_8py.html#accbd33fde294a4bc987dea62ae07dab7", null ],
    [ "dcol", "shares_8py.html#a4372df25c549d91225991730026db47c", null ],
    [ "enable", "shares_8py.html#a32aab2816e2ad6c19726371d01fb414f", null ],
    [ "enc1_pos", "shares_8py.html#aed9e9706cdded6da4acdbc65122d63ab", null ],
    [ "enc1_speed", "shares_8py.html#a943832ba36c9a81d9716df9e10b5edc7", null ],
    [ "enc2_pos", "shares_8py.html#a33415a5fd949107c393abe85072cd165", null ],
    [ "enc2_speed", "shares_8py.html#a24012ca16be8b72ccca04a1f0dd9ad57", null ],
    [ "eulerx", "shares_8py.html#a22e95b9c759007ccd8a923686c0ff637", null ],
    [ "eulery", "shares_8py.html#a860dd81705ea65250ff4ef537bae5595", null ],
    [ "eulerz", "shares_8py.html#a25fcc7a14fc208cf617b08b8afc09028", null ],
    [ "fault", "shares_8py.html#a5e87668559ec31751c5378e7943cf768", null ],
    [ "Kt", "shares_8py.html#a2556ce4f53decd02320d60189877c242", null ],
    [ "omegax", "shares_8py.html#a29d48640af6f24a796d3497aa845441e", null ],
    [ "omegay", "shares_8py.html#a2a5254ccef9e659694330d232b68f423", null ],
    [ "omegaz", "shares_8py.html#a95ad07ccbc7efb5788e69fece78c6766", null ],
    [ "op_mode", "shares_8py.html#a0b4c3465e41a148e099dc702e10455b8", null ],
    [ "pwmx", "shares_8py.html#ad56bb2d258de5482cf8498ae98928860", null ],
    [ "pwmy", "shares_8py.html#aac5264c5169219001ea225a0411fd3b2", null ],
    [ "R", "shares_8py.html#a9e68ce162b7ce47c6c7f3c23871a264b", null ],
    [ "thx", "shares_8py.html#a36d5cc03bfd38c92b79f10d2fe103a59", null ],
    [ "thx_d", "shares_8py.html#a165e08e674598c36fdefe40c681878cf", null ],
    [ "thy", "shares_8py.html#a581869dba0079bdeccfbc6a7909e4859", null ],
    [ "thy_d", "shares_8py.html#ac540394102bfafbfbc02d993fbe0ec30", null ],
    [ "User_Input", "shares_8py.html#ab2470f6301d37d0908fc3bcc733b7276", null ],
    [ "Vdc", "shares_8py.html#a911e30fdb0bac7ed03de357fbe9d2219", null ],
    [ "x", "shares_8py.html#aa3104f8fdb6091da6ec17acaa5fe0087", null ],
    [ "x_d", "shares_8py.html#a25f756fa92c354c6c4b22e375f7e0fd0", null ],
    [ "x_dpred", "shares_8py.html#a41a3437551e132f99cd746c269bb1875", null ],
    [ "xpred", "shares_8py.html#aecf567f8e90400eea661726082e3a284", null ],
    [ "y", "shares_8py.html#a29c058d83fed912720d2cc7e93ed8b43", null ],
    [ "y_d", "shares_8py.html#a5b47375723e042daddd9319d2fa57090", null ],
    [ "y_dpred", "shares_8py.html#aadde40bfe461a85ee564b1036a9ca86e", null ],
    [ "ypred", "shares_8py.html#aaacea15d01d1793c0e4f5e1390303342", null ],
    [ "z", "shares_8py.html#a12f5f6df4f776a17399222f33ee50394", null ],
    [ "zero_enc", "shares_8py.html#ab24ce14c19ee66a6253127680460a37c", null ]
];